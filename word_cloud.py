# -*- coding: utf-8 -*-
# python 2 base

import collections
import random
import pytagcloud # requires Korean font support
from analysis import data_read, remove_id, tagging, filtering
from math import sqrt, log

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


r = lambda: random.randint(100,240)
r2 = lambda: random.randint(150,210)
color = lambda: (r(), r2(), r())

def get_tags(nouns, ntags=50):
    count = collections.Counter(nouns)
    # sqrt(10)
    return [{ 'color': color(), 'tag': n, 'size':c/2 } for n, c in count.most_common(ntags)]

# def get_tags(count, ntags=50, multiplier=10):
#     return [{ 'color': color(), 'tag': n, 'size': c*multiplier }\
#                 for n, c in count.most_common(ntags)]


def draw_cloud(tags, filename, fontname='Noto Sans CJK', size=(2000, 4000)):
    pytagcloud.create_tag_image(tags, filename, fontname=fontname, size=size)

def make_word_cloud(tokens):
    tags = get_tags(tokens)
    draw_cloud(tags, 'wordcloud.png')


meaningful_1words = set(["좆", "헬", "돈", "년", "놈", "못", "일", "왜", "나", "난"])

flist = ["data.txt", "헬조선2.txt"]
data = data_read(flist)
data = remove_id(data)
tagged_data = tagging(data, "helljosun2")
filtered_data = filtering(tagged_data, allowed_pos=["Noun"])
# tokens = [t[0] for line in filtered_data for t in line]
tokens = [t[0] for line in filtered_data for t in line if len(t[0]) > 1 or t[0] in meaningful_1words]
# print(tokens)
# tokens = [t for t in tokens if t is not "헬조선" and t is not "헬조센"]

# for i in range(10):
#     print(tokens[i])

make_word_cloud(tokens)
