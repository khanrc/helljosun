# HellJosun analysis

Frequency & co-occurence analysis about hell-josun twitter text data

Based on python 3.4

워드 클라우드 라이브러리인 pytagcloud 가 pygame 를 사용하는데, pygame 이 python 3 을 지원하지 않음. 따라서 word_cloud.py 는 python 2.7 을 기반으로 한다.

## Results

### analysis

![result](result.png)


**No threshold + meaningless word removal**

![result2](result2.png)

### frequency plot

![freq-plot](freq_plot.png)

### word cloud

![word-cloud](wordcloud.png)

## Requirement
### analysis

* python 3.4
* ~~mecab-ko (은전한닢)~~ -> 트위터 형태소 분석기로 변경
* konlpy
* numpy
* networkx
* matplotlib

### word_cloud

* python 2.7
* pygame
* pytagcloud


