#coding: utf-8

__author__ = 'CJB'

# from konlpy.tag import Mecab
from konlpy.tag import Twitter
import collections
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import os
import pickle
import nltk
from pprint import pprint

# reload(sys)
# sys.setdefaultencoding('utf-8')

vocab_size = 100 # equal to network size N
num_of_data = 0
meaningful_1words = set(["좆", "헬", "돈", "년", "놈", "못", "일", "왜", "나", "난"])

def data_read(flist):
    ret = []
    for fn in flist:
        with open(fn) as f:
            for line in f:
                ret.append(line.strip())

    return ret

def remove_id(data):
    """
    remove retweet id
    """
    for i in range(len(data)):
        while data[i][0] == '@':
            data[i] = data[i][data[i].find(' ')+1:]

    return data

def tagging(data, pkl_fn):
    directory_name = "tagged_pickles"
    pos_tagger = Twitter() # 일단 twitter tagger 를 사용하자
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)

    path = "{}/{}.{}.pkl".format(directory_name, pkl_fn, pos_tagger.__class__.__name__)

    if os.path.exists(path):
        with open(path, 'rb') as f:
            ret = pickle.load(f)
    else:
        ret = [pos_tagger.pos(d, norm=True, stem=True) for d in data]
        with open(path, 'wb') as f:
            pickle.dump(ret, f, protocol=0)

    return ret

def filtering(tagged_data, allowed_pos):
    if allowed_pos == None:
        return tagged_data
    return [[d for d in line if len(list(filter(lambda x: d[1].startswith(x), allowed_pos)))] for line in tagged_data]

# filtered tagged
def build_dataset(tagged):
    global vocab_size
    all_tagged = [t for l in tagged for t in l]
    count = collections.Counter(all_tagged).most_common(vocab_size)

    dictionary = dict()
    for word, _ in count:
        if len(word[0]) == 1 and word[0] not in meaningful_1words:
            print(word)
            continue
        dictionary[word] = len(dictionary)
    vocab_size = len(dictionary) # vocab_size 가 줄어들음
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

    indexed_data = []
    for line in tagged:
        indexed_line = []
        for word in line:
            if word in dictionary:
                indexed_line.append(dictionary[word])
        indexed_data.append(indexed_line)

    # for line in indexed_data:
    #     for idx in line:
    #         print reverse_dictionary[idx][0],
    #     print

    return indexed_data, count, dictionary, reverse_dictionary

def normalize(value, type):
    if type=="node":
        return np.sqrt(value) * 100
    elif type=="edge":
        return value / (num_of_data / 100)
        # return np.sqrt(value) / 3
    return 0

def data_exploration(data):
    print(data)
    tokens = [t for line in data for t in line if len(t[0]) > 1 or t[0] in meaningful_1words]
    print(len(tokens)) # 24862
    text = nltk.Text(tokens, name='NMSC')
    print(text)
    print(len(text.tokens)) # num of tokens
    print(len(set(text.tokens))) # num of unique tokens
    # print(text.vocab().most_common(50))
    pprint(text.vocab().most_common(50))

    # 이렇게 하면 파일로 출력할 수는 있으나, 하단 x 축이 잘려버림.
    # myshow = lambda: plt.savefig('plot.png')
    # tmp = plt.show
    # plt.show = myshow
    text.plot(50)
    # plt.show = tmp


if __name__ == "__main__":
    # fn = "data.txt"
    flist = ["data.txt", "헬조선2.txt"]
    data = data_read(flist)
    print(len(data))
    data = list(set(data)) # remove duplicate
    data = remove_id(data)
    num_of_data = len(data)
    print(num_of_data)
    # tagging
    tagged_data = tagging(data, "helljosun2")

    # data_exploration(tagged_data)


    # collecting noun only
    # pos tag refer: https://docs.google.com/spreadsheets/d/1-9blXKjtjeKZqsf4NzHeYJCrr49-nXeRF6D80udfcwY/edit#gid=4
    # NNG 일반 명사
    # NNP 고유 명사
    # 합성어 chk : startswith 처리해놓음
    # noun_data = [[d for d in line if d[1].startswith("NNG") or d[1].startswith("NNP")] for line in tagged_data]

    # N 명사, V 용언 (동사, 형용사 등), M 관형사, 부사
    # filtered_data = filtering(tagged_data, =["NNP", "NNG"])
    filtered_data = filtering(tagged_data, allowed_pos=["Noun"])
    data_exploration(filtered_data)

    # build_dataset(tagged_data)
    indexed_data, count, dictionary, reverse_dictionary = build_dataset(filtered_data)

    # analysis
    node_count = np.zeros([vocab_size], dtype=np.int)
    edge_count = np.zeros([vocab_size, vocab_size], dtype=np.int)
    for line in indexed_data:
        ln = len(line)
        for i in range(ln):
            node_count[line[i]] += 1
            for j in range(i+1, ln):
                edge_count[line[i]][line[j]] += 1
                edge_count[line[j]][line[i]] += 1

    # print(node_count) # equal to count
    # print(edge_count)

    # visualization
    G = nx.Graph()
    node_size = {}
    for i in range(vocab_size):
        word_i = reverse_dictionary[i][0]
        node_size[word_i] = normalize(node_count[i], type="node")
        for j in range(i+1, vocab_size):
            if edge_count[i][j] > 10:
                word_j = reverse_dictionary[j][0]
                G.add_edge(word_i, word_j, weight=normalize(edge_count[i][j], type="edge"))

    nodes = G.nodes()
    edges = G.edges()
    edge_weights = [G[u][v]['weight'] for u,v in edges]
    node_size = [node_size[w] for w in nodes]

    pos = nx.spring_layout(G)
    nx.draw(G, pos, node_size=node_size, width=edge_weights, with_labels=True)

    plt.axis('off')
    # plt.savefig('result.png')
    plt.show()